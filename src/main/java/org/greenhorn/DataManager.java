/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greenhorn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author greenhorn
 */
public class DataManager {

    private Connection connect;
    private static final String USER = "greenhorn";
    private static final String PASSWORD = "greenhorn";
    private static final String DATABASENAME = "school";

    public void connect() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        // Setup the connection with the DB
        connect = DriverManager
                .getConnection("jdbc:mysql://localhost/" + DATABASENAME + "?user=" + USER + "&password=" + PASSWORD + "");
    }

    /**
     * Retrieve data from the given tableName.
     *
     * @param tableName
     * @throws SQLException
     */
    public void getAll(String tableName) throws SQLException {
        PreparedStatement selectStatment = connect.prepareStatement("SELECT * FROM "+tableName);
        ResultSet resultSet = selectStatment.executeQuery();
        writeResultSet(resultSet);
    }

    /**
     * Prints out the data in console.
     *
     * @param resultSet
     * @throws SQLException
     */
    public void writeResultSet(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            System.err.println("-------------------------");
            String id = resultSet.getString("ID");
            String firstName = resultSet.getString("FirstName");
            System.err.println("Id: " + id);
            System.err.println("FirstName: " + firstName);
        }

        System.err.println("-------------------------");
    }

    /**
     * Gracefuly close connection to database.
     *
     s* @throws SQLException
     */
    public void disconnect() throws SQLException {
        connect.close();
    }

}
