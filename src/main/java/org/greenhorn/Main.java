/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greenhorn;

/**
 *
 * @author greenhorn
 */
public class Main {

    public static void main(String[] args) throws Exception {
        DataManager dataManager = new DataManager();
        dataManager.connect();

        int iterations = 60;
        for (int i = 0; iterations > i; i++) {
            Thread.sleep(1000); // Sleep for 1 seconds.
            
            System.err.println("");
            System.err.println("[iteration]" + i);
            dataManager.getAll("student");
            System.err.println("");
        }

        dataManager.disconnect();
    }

}
